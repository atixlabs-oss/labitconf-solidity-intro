# LABITCONF - Intro to solidity

This repo contains a pretty basic example of an ERC20 using the Truffle framework for a talk in LABITCONF 2020.


DO NOT USE THIS IN PRODUCTION!


## How to


### Install

Having node at 12.19.0(we recommend using NVM for this) just run

`npm install`

### Test

Having installed the package run

`npm test`


### Deploy

We have set up two networks for you to deploy your contracts


#### Local

If you want to deploy the Smart Contract in a local Blockchain you can use

`npm run deploy-local`

Keep in mind that you will have to have a blockchain listening on the port 8545, you can do just this by running the following command in another terminal

`npm run ganache`


#### Kovan

If you want to deploy the Smart Contract to the Kovan testent you just have to run

`npm run deploy-kovan`

having MNEMONIC and KOVAN_NODE_URL set as env vars. You can do this by creating a file with the following content


```
KOVAN_NODE_URL=https://kovan.infura.io/v3/123123123123123123123123123
MNEMONIC=reduce negative dinner romance exchange suffer simple dream glimpse record grab index
```

The Mnemonic should belong to an account which has enough KETH(Ethers in the kovan network), we recommend doing this through metamask and any faucet you can find on the internet.

The URL can be obtained from Infura, it will be the entrypoint to the blockchain.
