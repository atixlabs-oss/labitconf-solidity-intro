pragma solidity 0.7.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

contract MyERC20Events {
    event CreatorUsed(address indexed creator, bytes callData);
}

contract MyERC20 is ERC20, MyERC20Events {
    using SafeMath for uint256;
    
    string public greeting;
    address public creator;

    constructor(string memory _greeting) ERC20("LaBitconf", "LBC") {
        greeting = _greeting;
        creator = _msgSender();
        _mint(_msgSender(), 10 ** 18);
    }

    modifier creatorLog() {
        if(_msgSender() == creator) emit CreatorUsed(creator, msg.data);
        _;
    }

    function greet() public view returns(string memory) { // This is redundant
        return greeting;
    }


    function sum(uint256 aNumber, uint256 anotherNumber) public pure returns (uint256) {
        return aNumber.add(anotherNumber);
    }

    function transfer(address to, uint256 value) override public creatorLog returns (bool) {
        return ERC20.transfer(to, value);
    }
}

