const Migrations = artifacts.require('Migrations');

module.exports = function deployFirstStep(deployer) {
  deployer.deploy(Migrations);
};
