const chai = require('chai');
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const { web3 } = require('@openzeppelin/test-helpers/src/setup');

const MyERC20 = artifacts.require('MyERC20');

chai.use(require('chai-bn')(BN));

const { expect } = chai;

contract('GIVEN a myERC20 has been deployed', function ([firstAccount, secondAccount]) {
  before(async function () {
    this.myERC20 = await MyERC20.deployed();
  });
  describe('WHEN someone tries to add two numbers', function () {
    it('THEN the sum is correct', async function () {
      expect(await this.myERC20.sum(1, 2)).to.be.a.bignumber.that.equals(new BN(3));
    });
  });

  describe('WHEN someone tells the contract to greet', function () {
    it('THEN the contract happily greets the LABITCONF', async function () {
      expect(await this.myERC20.greeting()).to.be.equal('Hello LABITCONF');
    });
  });

  describe('WHEN someone calls the greet function', function () {
    it('THEN the contract returns the set greeting', async function () {
      expect(await this.myERC20.greeting()).to.be.equal(await this.myERC20.greet());
    });
  });

  describe('WHEN someone asks for the creator of the contract', function () {
    it('THEN the contract returns the first address we set', async function () {
      expect(await this.myERC20.creator()).to.be.equal(firstAccount);
    });
  });

  describe('WHEN someone asks for balance of the creator to the contract', function () {
    it('THEN the contract returns the equivalent to a token', async function () {
      expect(await this.myERC20.balanceOf(firstAccount)).to.be.a.bignumber.that.equals(
        new BN('1000000000000000000')
      );
    });
  });

  describe('WHEN someone asks for balance of another account the contract', function () {
    it('THEN the contract returns zero balance', async function () {
      expect(await this.myERC20.balanceOf(secondAccount)).to.be.a.bignumber.that.equals(new BN(0));
    });
  });
});

contract('GIVEN a myERC20 has been deployed', function ([firstAccount, secondAccount]) {
  // eslint-disable-next-line
  before(async function () {
    this.myERC20 = await MyERC20.deployed();
  });
  describe('WHEN the creator sends tokens to another account', function () {
    before(async function () {
      this.transferedAmount = new BN('50000000000');
      this.tx = await this.myERC20.transfer(secondAccount, this.transferedAmount);
    });
    it('THEN the second account has the transfered amount as balance', async function () {
      expect(await this.myERC20.balanceOf(secondAccount)).to.be.a.bignumber.that.equals(
        this.transferedAmount
      );
    });
    it('THEN the first account has the balance decreased exactly by the transfered amount', async function () {
      expect(await this.myERC20.balanceOf(firstAccount)).to.be.a.bignumber.that.equals(
        new BN('1000000000000000000').sub(this.transferedAmount)
      );
    });
    it('THEN the contract emitted a Transfer event', async function () {
      expectEvent(this.tx, 'Transfer', {
        from: firstAccount,
        to: secondAccount,
        value: this.transferedAmount,
      });
    });

    it('THEN the contract emitted an event telling the owner used it', async function () {
      const tx = await web3.eth.getTransaction(this.tx.tx);
      expectEvent(this.tx, 'CreatorUsed', {
        creator: firstAccount,
        callData: tx.input,
      });
    });
  });
});

contract('GIVEN a myERC20 has been deployed', function ([, secondAccount, thirdAccount]) {
  // eslint-disable-next-line
  before(async function () {
    this.myERC20 = await MyERC20.deployed();
  });
  describe('WHEN someone without balance sends tokens to another account', function () {
    it('THEN the tx fails', async function () {
      this.transferedAmount = new BN('50000000000');
      return expectRevert(
        this.myERC20.transfer(thirdAccount, this.transferedAmount, { from: secondAccount }),
        'ERC20: transfer amount exceeds balance'
      );
    });
  });
});
